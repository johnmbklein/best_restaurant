import java.util.List;
import org.junit.*;
import static org.junit.Assert.*;
import org.sql2o.*;

public class RestaurantTest {

  @Before
  public void setUp() {
    DB.sql2o = new Sql2o("jdbc:postgresql://localhost:5432/best_restaurant_test", null, null);
  }

  @After
  public void tearDown() {
    try(Connection con = DB.sql2o.open()) {
      String deleteTasksQuery = "DELETE FROM restaurants *;";
      String deleteCategoriesQuery = "DELETE FROM cuisines *;";
      con.createQuery(deleteTasksQuery).executeUpdate();
      con.createQuery(deleteCategoriesQuery).executeUpdate();
    }
  }

  @Test
  public void restaurant_instantiatesCorrectly_true() {
    Restaurant testRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    assertEquals(true, testRestaurant instanceof Restaurant);
  }

  @Test
  public void getName_instantiatesWithName_String() {
    Restaurant testRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    assertEquals("Nicolas", testRestaurant.getName());
  }

  @Test
  public void getAddress_instantiatesWithAddress_String() {
    Restaurant testRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    assertEquals("123 NE Grand Ave., Portland, OR, 97210", testRestaurant.getAddress());
  }

  @Test
  public void getHours_instantiatesWithHours_String() {
    Restaurant testRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    assertEquals("10:00AM - 9:00PM", testRestaurant.getHours());
  }

  @Test
  public void getCuisineId_instantiatesWithCuisineId_int() {
    Restaurant testRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    assertEquals(4, testRestaurant.getCuisineId());
  }


  @Test
  public void all_emptyAtFirst() {
    assertEquals(Restaurant.all().size(), 0);
  }

  @Test
  public void all_returnsSavedRestaurants() {
    Restaurant firstRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    Restaurant secondRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    firstRestaurant.save();
    secondRestaurant.save();
    assertEquals(Restaurant.all().size(), 2);
  }

  @Test
  public void equals_returnsTrueIfMemberVariablesAretheSame() {
    Restaurant firstRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    Restaurant secondRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    assertTrue(firstRestaurant.equals(secondRestaurant));
  }

  @Test
  public void find_findRestaurantInDatabase_true() {
    Restaurant testRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    testRestaurant.save();
    Restaurant savedRestaurant = Restaurant.find(testRestaurant.getId());
    assertTrue(testRestaurant.equals(savedRestaurant));
  }

  @Test
  public void save_assignsIdToObject_int() {
    Restaurant testRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    testRestaurant.save();
    Restaurant savedRestaurant = Restaurant.all().get(0);
    assertEquals(testRestaurant.getId(), savedRestaurant.getId());
  }

  @Test
  public void save_savesIntoDatabase_true() {
    Restaurant testRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", 4);
    testRestaurant.save();
    assertTrue(Restaurant.all().get(0).equals(testRestaurant));
  }

  @Test
  public void find_returnsNullWhenNoTaskFound_null() {
    assertTrue(Restaurant.find(999) == null);
  }

}
