import org.fluentlenium.adapter.FluentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.*;
import org.junit.*;
import java.util.List;
import org.sql2o.*;

public class AppTest extends FluentTest {
  public WebDriver webDriver = new HtmlUnitDriver();
  @Before
  public void setUp() {
    DB.sql2o = new Sql2o("jdbc:postgresql://localhost:5432/best_restaurant_test", null, null);
  }

  @After
  public void tearDown() {
    try(Connection con = DB.sql2o.open()) {
      String deleteTasksQuery = "DELETE FROM restaurants *;";
      String deleteCategoriesQuery = "DELETE FROM cuisines *;";
      con.createQuery(deleteTasksQuery).executeUpdate();
      con.createQuery(deleteCategoriesQuery).executeUpdate();
    }
  }

  @Override
  public WebDriver getDefaultDriver() {
    return webDriver;
  }

  @ClassRule
  public static ServerRule server = new ServerRule();

  @Test
  public void rootTest() {
    goTo("http://localhost:4567/");
    assertThat(pageSource()).contains("Not Yelp");
  }

  @Test
  public void cuisineIsCreatedTest(){
    goTo("http://localhost:4567/");
    click("a", withText("Add a New Cuisine"));
    fill("#newCuisine").with("French");
    submit(".btn");
    assertThat(pageSource()).contains("French");
  }

  @Test
  public void cuisineIsDisplayedTest() {
    Cuisine myCuisine = new Cuisine("Italian");
    myCuisine.save();
    String cuisinePath = String.format("http://localhost:4567/cuisines/%d", myCuisine.getId());
    goTo(cuisinePath);
    assertThat(pageSource()).contains("Italian");
  }

  @Test
  public void restaurantIsCreatedAndDisplayed() {
    goTo("http://localhost:4567/cuisines/new");
    fill("#newCuisine").with("Spanish");
    submit(".btn");
    click("a", withText("Spanish"));
    click("a", withText("Add a new restaurant in Spanish"));
    fill("#name").with("Gorditos");
    fill("#address").with("1234 SE Division");
    fill("#hours").with("noon to 6pm");
    submit(".btn");
    assertThat(pageSource()).contains("Gorditos");
  }

  @Test
  public void allRestaurantsDisplayNameOnCuisinePage() {
    Cuisine myCuisine = new Cuisine("Bars");
    myCuisine.save();
    Restaurant firstRestaurant = new Restaurant("Otto's", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", myCuisine.getId());
    Restaurant secondRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", myCuisine.getId());
    firstRestaurant.save();
    secondRestaurant.save();
    String cuisinePath = String.format("http://localhost:4567/cuisines/%d", myCuisine.getId());
    goTo(cuisinePath);
    assertThat(pageSource()).contains("Otto's");
    assertThat(pageSource()).contains("Nicolas");
 }

 @Test
 public void newCuisinePageDisplayed() {
   goTo("http://localhost:4567/cuisines/new");
   assertThat(pageSource()).contains("Add a Cuisine");
 }

 @Test
 public void newRestaurantPageDisplayed() {
   Cuisine myCuisine = new Cuisine("Bars");
   myCuisine.save();
   String cuisinePath = String.format("http://localhost:4567/cuisines/%d/restaurants/new", myCuisine.getId());
   goTo(cuisinePath);
   assertThat(pageSource()).contains("Add a restaurant to Bars");
 }

 @Test
 public void restaurantPageDisplaysRestaurants() {
   Cuisine myCuisine = new Cuisine("Bars");
   myCuisine.save();
   Restaurant myRestaurant = new Restaurant("Boom Boom Room", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", myCuisine.getId());
   myRestaurant.save();
   String restaurantPath = String.format("http://localhost:4567/restaurants/%d/", myRestaurant.getId());
   goTo(restaurantPath);
   assertThat(pageSource()).contains("Boom Boom Room");
 }

  @Test
  public void restaurantNotFoundMessageShown() {
    goTo("http://localhost:4567/restaurants/999");
    assertThat(pageSource()).contains("Restaurant not found");
  }
}
