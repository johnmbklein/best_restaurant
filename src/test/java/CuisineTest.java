import org.sql2o.*;
import org.junit.*;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;

public class CuisineTest {

  @Before
  public void setUp() {
    DB.sql2o = new Sql2o("jdbc:postgresql://localhost:5432/best_restaurant_test", null, null);
  }

  @After
  public void tearDown() {
    try(Connection con = DB.sql2o.open()) {
      String deleteTasksQuery = "DELETE FROM restaurants *;";
      String deleteCategoriesQuery = "DELETE FROM cuisines *;";
      con.createQuery(deleteTasksQuery).executeUpdate();
      con.createQuery(deleteCategoriesQuery).executeUpdate();
    }
  }

  @Test
  public void Cuisine_instantiatesCorrestly_true() {
    Cuisine testCuisine = new Cuisine("Mexican");
    assertEquals(true, testCuisine instanceof Cuisine);
  }

  @Test
  public void getName_returnsName_String() {
    Cuisine testCuisine = new Cuisine ("Cuban");
    assertEquals("Cuban", testCuisine.getName());
  }


  @Test
  public void all_emptyAtFirst() {
    assertEquals(Cuisine.all().size(), 0);
  }

  @Test
  public void equals_returnsTrueIfNamesAretheSame() {
    Cuisine firstCuisine = new Cuisine("Mexican");
    Cuisine secondCuisine = new Cuisine("Mexican");
    assertTrue(firstCuisine.equals(secondCuisine));
  }

  @Test
  public void save_savesIntoDatabase_true() {
    Cuisine testCuisine = new Cuisine("Mexican");
    testCuisine.save();
    assertTrue(Cuisine.all().get(0).equals(testCuisine));
  }

  @Test
  public void save_assignsIdToObject_true() {
    Cuisine testCuisine = new Cuisine("French");
    testCuisine.save();
    Cuisine savedCuisine = Cuisine.all().get(0);
    assertEquals(testCuisine.getId(), savedCuisine.getId());
  }

  @Test
  public void find_findCuisineInDatabase_true() {
    Cuisine testCuisine = new Cuisine("Seafood");
    testCuisine.save();
    Cuisine savedCuisine = Cuisine.find(testCuisine.getId());
    assertTrue(testCuisine.equals(savedCuisine));
  }

  @Test
  public void getRestaurants_retrievesALlRestaurantsFromDatabase_tasksList() {
    Cuisine testCuisine = new Cuisine("French");
    testCuisine.save();
    Restaurant firstRestaurant = new Restaurant("Otto's", "123 SE Woodstock Ave., Portland, OR, 97210", "9:00AM - 6:00PM", testCuisine.getId());
    Restaurant secondRestaurant = new Restaurant("Nicolas", "123 NE Grand Ave., Portland, OR, 97210", "10:00AM - 9:00PM", testCuisine.getId());
    firstRestaurant.save();
    secondRestaurant.save();
    Restaurant[] restaurants = new Restaurant[] { firstRestaurant, secondRestaurant };
    assertTrue(testCuisine.getRestaurants().containsAll(Arrays.asList(restaurants)));
  }
}
