import java.util.List;
import org.sql2o.*;

public class Restaurant {
  private int id;
  private String name;
  private String address;
  private String hours;
  private int cuisine_id;

  public Restaurant(String name, String address, String hours, int cuisine_id) {
    this.name = name;
    this.address = address;
    this.hours = hours;
    this.cuisine_id = cuisine_id;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public String getHours() {
    return hours;
  }

  public int getCuisineId() {
    return cuisine_id;
  }

  public static List<Restaurant> all() {
    String sql = "SELECT * FROM restaurants";
    try(Connection con = DB.sql2o.open()) {
      return con.createQuery(sql).executeAndFetch(Restaurant.class);
    }
  }

  @Override
  public boolean equals(Object otherRestaurant) {
    if (!(otherRestaurant instanceof Restaurant)) {
      return false;
    } else {
      Restaurant newRestaurant = (Restaurant) otherRestaurant;
      return this.getName().equals(newRestaurant.getName()) &&
      this.getAddress().equals(newRestaurant.getAddress()) &&
      this.getHours().equals(newRestaurant.getHours()) &&
      this.getId() == newRestaurant.getId() &&
      this.getCuisineId() == newRestaurant.getCuisineId();
    }
  }

  public void save() {
    try(Connection con = DB.sql2o.open()) {
      String sql = "INSERT INTO restaurants(name, address, hours, cuisine_id) VALUES (:name, :address, :hours, :cuisine_id)";
      this.id = (int) con.createQuery(sql, true)
        .addParameter("name", this.name)
        .addParameter("address", this.address)
        .addParameter("hours", this.hours)
        .addParameter("cuisine_id", this.cuisine_id)
        .executeUpdate()
        .getKey();
    }
  }

  public static Restaurant find(int id) {
    try(Connection con = DB.sql2o.open()) {
      String sql = "SELECT * FROM restaurants where id=:id";
      Restaurant newRestaurant = con.createQuery(sql)
        .addParameter("id", id)
        .executeAndFetchFirst(Restaurant.class);
      return newRestaurant;
    }
  }


}
