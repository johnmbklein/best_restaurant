import java.util.List;
import java.util.HashMap;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import static spark.Spark.*;

public class App {
  public static void main(String[] args) {
    staticFileLocation("/public");
    String layout = "templates/layout.vtl";

    //CREATE
    post("/cuisines/new/success", (request, response) -> {
      HashMap<String, Object> model = new HashMap<String, Object>();
      String cuisineName = request.queryParams("newCuisine");
      Cuisine newCuisine = new Cuisine(cuisineName);
      newCuisine.save();
      response.redirect("/cuisines");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    post("/restaurants/new/success", (request, response) -> {
      HashMap<String, Object> model = new HashMap<String, Object>();
      String restaurantName = request.queryParams("name");
      String restaurantAddress = request.queryParams("address");
      String restaurantHours = request.queryParams("hours");
      int cuisineId = Integer.parseInt(request.queryParams("cuisineId"));
      Restaurant newRestaurant = new Restaurant(restaurantName, restaurantAddress, restaurantHours, cuisineId);
      newRestaurant.save();
      String redirectAddress = "/cuisines/" + Integer.parseInt(request.queryParams("cuisineId"));
      response.redirect(redirectAddress);
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    //READ
    get("/", (request, response) -> {
      HashMap<String, Object> model = new HashMap<String, Object>();
      model.put("template", "templates/index.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    get("/cuisines/new", (request, response) -> {
      HashMap<String,Object> model = new HashMap<String, Object>();
      model.put("template", "templates/new-cuisine-form.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());


    get("/cuisines", (request, response) -> {
      HashMap<String,Object> model = new HashMap<String,Object>();
      model.put("cuisines", Cuisine.all());
      model.put("template", "templates/cuisines.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    get("/cuisines/:id", (request, response) -> {
      HashMap<String,Object> model = new HashMap<String,Object>();
      Cuisine cuisine = Cuisine.find(Integer.parseInt(request.params(":id")));
      model.put("cuisine", cuisine);
      model.put("template", "templates/cuisine.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    get("cuisines/:id/restaurants/new", (request, response) -> {
      HashMap<String,Object> model = new HashMap<String,Object>();
        Cuisine cuisine = Cuisine.find(Integer.parseInt(request.params(":id")));
        model.put("cuisine", cuisine);
        model.put("template", "templates/cuisine-restaurant-form.vtl");
        return new ModelAndView(model, layout);
      }, new VelocityTemplateEngine());

    get("/restaurants", (request, response) -> {
      HashMap<String,Object> model = new HashMap<String,Object>();
      model.put("restaurants", Restaurant.all());
      model.put("template", "templates/restaurants.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

    get("/restaurants/:id", (request, response) -> {
      HashMap<String,Object> model = new HashMap<String,Object>();
      Restaurant restaurant = Restaurant.find(Integer.parseInt(request.params(":id")));
      model.put("restaurant", restaurant);
      model.put("template", "templates/restaurant.vtl");
      return new ModelAndView(model, layout);
    }, new VelocityTemplateEngine());

  }
}
